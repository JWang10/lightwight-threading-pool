# Lightweight Threading Pool

a lightweight threading pool with Goroutine from scratch in Go.

### Concept 

from the image below

![img.png](assets/img.png)


There is a struct Pool, including the components from the image above.

```go
type Task func()

type Pool struct {
	capacity int            // size of worker pool 
	
	task    chan Task       // task channel
	active  chan struct{}   // active channel
	
	wg      sync.WaitGroup  // it doesn't be destroyed until the workers quit
	quit    chan struct{}   // a signal to quit for the workers
}

```

### Demo

- demo1 drawback
  - 如果`workerpool`中`worker`數量滿了，而且`worker`都在處理task時
    - 此情況會導致用戶調用`Schedule`方法時堵塞，
![img.png](assets/demo1.png)

- demo2
  - 採用功能選項機制 (functional option)
    - 此機制，可以讓包內用戶根據自己的需求，通過不同功能選項來指定包的行為
  ![img.png](assets/demo2.png)