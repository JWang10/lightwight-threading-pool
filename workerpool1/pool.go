package workerpool

import (
	"errors"
	"fmt"
	"sync"
)

var (
	ErrNoIdleWorkerInPool = errors.New("no idle worker in pool")
	ErrWorkerPoolFreed    = errors.New("workerpool freed")
)

const (
	defaultCapacity = 10
	maxCapacity     = 100
)

type Task func()

type Pool struct {
	capacity int

	task   chan Task
	active chan struct{}

	wg   sync.WaitGroup
	quit chan struct{}
}

func New(capacity int) *Pool {
	if capacity < 0 {
		capacity = defaultCapacity
	}
	if capacity > maxCapacity {
		capacity = maxCapacity
	}

	p := &Pool{
		capacity: capacity,
		task:     make(chan Task),
		active:   make(chan struct{}, capacity),
		quit:     make(chan struct{}),
	}

	println("workerpool start")
	go p.run()
	return p
}

func (p *Pool) run() {
	idx := 0

	for {
		select {
		case <-p.quit:
			return
		case p.active <- struct{}{}:
			// create a new worker
			idx++
			p.newWorker(idx)
		}
	}
}

func (p *Pool) newWorker(idx int) {
	p.wg.Add(1)
	go func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("worker[%03d]: recover panic[%s] and exit\n", idx, err)
				<-p.active
			}
			p.wg.Done() // 減少waitgroup數量
		}()
		fmt.Printf("worker[%03d]: start\n", idx)

		for {
			select {
			case <-p.quit:
				fmt.Printf("worker[%03d]: exit\n", idx)
				<-p.active // worker quit and reduce the number of active worker
				return
			case t := <-p.task: // prevent panic
				fmt.Printf("worker[%03d]: receive a task\n", idx)
				t()
			}
		}
	}()
}

func (p *Pool) Schedule(t Task) error {
	select {
	case <-p.quit:
		return ErrWorkerPoolFreed
	case p.task <- t:
		return nil
	}
}

func (p *Pool) Free() {
	close(p.quit)
	p.wg.Wait()
	fmt.Println("workerpool freed")
}
