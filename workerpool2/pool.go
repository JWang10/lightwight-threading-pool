package workerpool

import (
	"errors"
	"fmt"
	"sync"
)

var (
	ErrNoIdleWorkerInPool = errors.New("no idle worker in pool")
	ErrWorkerPoolFreed    = errors.New("workerpool freed")
)

const (
	defaultCapacity = 10
	maxCapacity     = 100
)

type Task func()

type Pool struct {
	capacity int
	preAlloc bool // 指創建pool時，預先創建workers。 default: false
	// 當pool滿的情況，新Schedule調用是否會堵塞當前goroutine。 default: true
	// 如果block = false，則Schedule返回ErrNoWorkerAvailInpool
	block  bool
	task   chan Task
	active chan struct{}

	wg   sync.WaitGroup
	quit chan struct{}
}

func New(capacity int, opts ...Option) *Pool {
	if capacity < 0 {
		capacity = defaultCapacity
	}
	if capacity > maxCapacity {
		capacity = maxCapacity
	}

	p := &Pool{
		capacity: capacity,
		task:     make(chan Task),
		active:   make(chan struct{}, capacity),
		quit:     make(chan struct{}),
	}

	for _, opt := range opts {
		opt(p)
	}

	fmt.Printf("workerpool start(preAlloc=%t)\n", p.preAlloc)

	if p.preAlloc {
		// create all goroutines and send into workers channel
		for i := 0; i < p.capacity; i++ {
			p.newWorker(i + 1)
			p.active <- struct{}{}
		}
	}

	go p.run()
	return p
}

func (p *Pool) run() {
	idx := len(p.active)

	if !p.preAlloc {
	loop:
		for t := range p.task {
			p.returnTask(t)
			select {
			case <-p.quit:
				return
			case p.active <- struct{}{}:
				idx++
				p.newWorker(idx)
			default:
				break loop
			}
		}
	}

	for {
		select {
		case <-p.quit:
			return
		case p.active <- struct{}{}:
			// create a new worker
			idx++
			p.newWorker(idx)
		}
	}
}

func (p *Pool) newWorker(idx int) {
	p.wg.Add(1)
	go func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("worker[%03d]: recover panic[%s] and exit\n", idx, err)
				<-p.active
			}
			p.wg.Done() // 減少waitgroup數量
		}()
		fmt.Printf("worker[%03d]: start\n", idx)

		for {
			select {
			case <-p.quit:
				fmt.Printf("worker[%03d]: exit\n", idx)
				<-p.active // worker quit and reduce the number of active worker
				return
			case t := <-p.task: // prevent panic
				fmt.Printf("worker[%03d]: receive a task\n", idx)
				t()
			}
		}
	}()
}

func (p *Pool) Schedule(t Task) error {
	select {
	case <-p.quit:
		return ErrWorkerPoolFreed
	case p.task <- t:
		return nil
	default:
		if p.block {
			p.task <- t
			return nil
		}
		return ErrNoIdleWorkerInPool
	}
}

func (p *Pool) Free() {
	close(p.quit)
	p.wg.Wait()
	fmt.Printf("workerpool freed(preAlloc=%t)\n", p.preAlloc)
}

func (p *Pool) returnTask(t Task) {
	go func() {
		p.task <- t
	}()
}
